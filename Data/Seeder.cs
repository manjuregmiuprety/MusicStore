using System.Collections.Generic;
using System.Linq;
using MusicStore.Models;

namespace MusicStore.Data
{
    public class Seeder
    {
        private ApplicationDbContext _context;
        public Seeder(ApplicationDbContext context)
        {
            _context = context;
        }
        public void CreateSongLanguage()
        {
            var languages = new List<Language>
            {
                new Language { Name = "English"},
                new Language { Name = "Hindi"},
                new Language { Name = "Nepali"}
            };
            
            if(_context.Languages.Any()){
                return;
            }

            _context.Languages.AddRange(languages);
            _context.SaveChanges();
        }
    }
}