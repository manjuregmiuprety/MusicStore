using System.Collections.Generic;
using System.Linq;
using Microsoft.EntityFrameworkCore;
using MusicStore.Data;
using MusicStore.Models;

namespace MusicStore.Repository
{
    public class SongRepository : ISongRepository
    {
        private ApplicationDbContext _context;
        public SongRepository(ApplicationDbContext context)
        {
            _context = context;
        }
        public void Create(Song song)
        {
            _context.Songs.Add(song);
            _context.SaveChanges();
        }

        public void Delete(Song song)
        {
            _context.Songs.Remove(song);
            _context.SaveChanges();
        }

        public List<Song> GetAllSongs()
        {
            return _context.Songs.Include(l=>l.Language)
                                .ToList();
        }

        public Song GetSingleSong(int Id)
        {
            return _context.Songs.FirstOrDefault(s=>s.Id==Id);
        }

        public List<Song> GetSongByLanguage(int languageId)
        {
            return _context.Songs.Include(l=>l.Language)
                                .Where(l=>l.LanguageId==languageId).ToList();
        }

        public List<Language> GetSongLanguage()
        {
            return _context.Languages.ToList();
        }

        public void Update(Song song)
        {
            _context.Songs.Update(song);
            _context.SaveChanges();
        }

        
    }
}