using System.Collections.Generic;
using System.Linq;
using MusicStore.Data;
using MusicStore.Models;

namespace MusicStore.Repository
{
    public class LanguageRepository : ILanguageRepository
    {
        private ApplicationDbContext _context;
        public LanguageRepository(ApplicationDbContext context)
        {
            _context = context;
        }
        public void Create(Language language)
        {
            _context.Languages.Add(language);
            _context.SaveChanges();
        }

        public void Delete(Language language)
        {
            _context.Languages.Remove(language);
            _context.SaveChanges();
        }

        public List<Language> GetAllLanguage()
        {
            return _context.Languages.ToList();
        }

        public Language GetSingleLanguage(int Id)
        {
            return _context.Languages.FirstOrDefault(l=>l.Id==Id);
        }
    }
}