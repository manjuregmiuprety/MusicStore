using System.Collections.Generic;
using MusicStore.Models;

namespace MusicStore.Repository
{
  public interface ILanguageRepository
  {
      void Create(Language language);
      void Delete(Language language);
      List<Language> GetAllLanguage();
      Language GetSingleLanguage(int Id);
  }
}