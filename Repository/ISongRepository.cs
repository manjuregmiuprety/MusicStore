using System.Collections.Generic;
using MusicStore.Models;

namespace MusicStore.Repository
{
    public interface ISongRepository
    {
        void Create(Song song);
        void Update(Song song);
        void Delete(Song song);
        List<Song> GetAllSongs();
        Song GetSingleSong(int Id);
        List<Language> GetSongLanguage();
        List<Song> GetSongByLanguage(int languageId);
    }
}