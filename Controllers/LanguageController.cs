using Microsoft.AspNetCore.Mvc;
using MusicStore.Repository;

namespace MusicStore.Controllers
{
    public class LanguageController : Controller
    {
        private ILanguageRepository _languageRepository;
        public LanguageController(ILanguageRepository languageRepository)
        {
            _languageRepository = languageRepository;
        }
        
        public IActionResult Index()
        {
            return View(_languageRepository.GetAllLanguage());
        }
    }
}