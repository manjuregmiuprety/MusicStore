using System.Collections.Generic;
using AutoMapper;
using ClientNotifications;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using MusicStore.Models;
using MusicStore.Repository;
using static ClientNotifications.Helpers.NotificationHelper;

namespace MusicStore.Controllers
{
    public class SongController : Controller
    {
        private ISongRepository _songRepository;
        private ILanguageRepository _languageRepository;
        private IClientNotification _clientNotification;
        private IMapper _mapper;
        public SongController(ISongRepository songRepository,
                            ILanguageRepository languageRepository,
                            IClientNotification clientNotification,
                            IMapper mapper)
        {
            _songRepository = songRepository;
            _languageRepository = languageRepository;
            _clientNotification = clientNotification;
            _mapper = mapper;
        }

        public IActionResult Index(int LanguageId=0)
        {
            var songs = new List<Song>();
            if(LanguageId==0){
                songs = _songRepository.GetAllSongs();
            }else
            {
                songs = _songRepository.GetSongByLanguage(LanguageId);
            }
            var languages = _songRepository.GetSongLanguage();
            ViewBag.songLanguage = new SelectList(languages, nameof(Language.Id), nameof(Language.Name));
           return View(songs);          
        }
        [HttpGet]
        public IActionResult New()
        {
            var songvm = new SongVm();
            songvm.IsEditMode = "false";

            var languages = _songRepository.GetSongLanguage();
            ViewBag.songLanguage = new SelectList(languages, nameof(Language.Id), nameof(Language.Name));
            return View(songvm);
        }
        [HttpPost]
        public IActionResult New(SongVm songvm)
        {
            var song = _mapper.Map<Song>(songvm);

            if(songvm.IsEditMode.Equals("false")){
                _songRepository.Create(song);
                _clientNotification.AddToastNotification("New Song Created",
                                            NotificationType.success,
                                            null);
            }else
            {
                _songRepository.Update(song);
                _clientNotification.AddToastNotification("Song Updated",
                                            NotificationType.success,
                                            null);
            }
            return RedirectToAction("Index");
        }

        public IActionResult Edit(int Id)
        {
            var songs = _songRepository.GetSingleSong(Id);

            var songVm = _mapper.Map<SongVm>(songs);

            songVm.IsEditMode="true";

            var languages = _songRepository.GetSongLanguage();
            ViewBag.songLanguage = new SelectList(languages, nameof(Language.Id), nameof(Language.Name));

            return View("New",songVm);
        }
        public IActionResult Delete(int Id)
        {
            var song = _songRepository.GetSingleSong(Id);
            _songRepository.Delete(song);
            _clientNotification.AddToastNotification("Succesfully Deleted",
                                            NotificationType.success,
                                            null);
            return RedirectToAction("Index");
        }
    }
}