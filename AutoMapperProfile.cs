using AutoMapper;
using MusicStore.Models;

namespace MusicStore
{
    public class AutoMapperProfile : Profile
    {
      public AutoMapperProfile()
      {
        CreateMap<Song , SongVm>().ReverseMap();
      }
    }
}