using System.Collections.Generic;

namespace MusicStore.Models
{
    public class Language
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public List<Song> Song { get; set; }
    }
}