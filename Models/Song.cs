namespace MusicStore.Models
{
    public class Song
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Singer { get; set; }
        public Language Language { get; set; }
        public int LanguageId { get; set; }
    }
}